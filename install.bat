@setlocal enableextensions
@cd /d "%~dp0"

echo off
cls
tasklist /fi "imagename eq DMedia.exe" |find ":" > nul
if errorlevel 1 (
	taskkill /f /im "DMedia.exe"
	echo Process DMedia.exe terminated

) else (
	echo No process DMedia.exe found
)
echo.
echo copying DMedia.exe to ProgramFiles...
copy ".\bin\Release\DMedia.exe" "%HOMEDRIVE%\Program Files (x86)\ASUS\ATK Package\ATK Media\"
echo.
if not exist "%USERPROFILE%\DMedia.ini" (
	echo copying DMedia.ini to UserProfile...
	copy ".\DMedia.ini" "%USERPROFILE%\DMedia.ini"
) else (
	echo DMedia.ini not copied to UserProfile ^(already exists^)
)
echo.
echo Done... Please restart your computer or run bin\Release\DMedia.exe manually
echo.
PAUSE
