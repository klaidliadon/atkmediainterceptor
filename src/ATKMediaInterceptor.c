#define _WIN32_WINNT 0x0500

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * 2011, zaak404@gmail.com
 *
 * keybd_event quick'n'dirty version: 3mptylabs@gmail.com
 * changes by dvaz
 */

#define MAX_CLASS_NAME 128

HINSTANCE hInst;
TCHAR szTitle[] = "ATKMEDIA";
TCHAR szWindowClass[] = "ATKMEDIA";

ATOM MRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE, int);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL LoadSettings();

#define SIZE 2048

char filename[SIZE];

char c_cmd[SIZE];
char space_cmd[SIZE];
char v_cmd[SIZE];
char enter_cmd[SIZE];

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
    MSG msg;

    char* home;
    home = getenv("USERPROFILE");
    strcpy (filename, home);
    strcat (filename, "\\DMedia.ini");

    GetPrivateProfileString("enter", "cmd", "calc.exe",                 enter_cmd, SIZE, filename);
    GetPrivateProfileString("space", "cmd", "control.exe powercfg.cpl", space_cmd, SIZE, filename);
    GetPrivateProfileString("c",     "cmd", "control.exe desktop",      c_cmd,     SIZE, filename);
    GetPrivateProfileString("v",     "cmd", "control.exe printers",     v_cmd,     SIZE, filename);

    printf("File:\t%s\n\nC:\t%s\nV:\t%s\nSpace:\t%s\nEnter:\t%s\n", filename, c_cmd, v_cmd, space_cmd, enter_cmd);

	MRegisterClass(hInstance);

	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int) msg.wParam;
}

ATOM MRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= 0;//LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= 0;//LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   return TRUE;
}

void RunProc(LPSTR path) {
	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

	CreateProcess(NULL, path, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);

	WaitForSingleObject( pi.hProcess, INFINITE );
    CloseHandle( pi.hProcess );
    CloseHandle( pi.hThread );
}

#define ATKMEDIA_MESSAGE				  0x0917
#define ATKMEDIA_PLAY                     0x0002
#define ATKMEDIA_STOP                     0x0003
#define ATKMEDIA_PREV                     0x0005
#define ATKMEDIA_NEXT                     0x0004
#define ATKMEDIA_ENTER                    0x002B

#define ATKMEDIA_SPACE                    0x005C
#define ATKMEDIA_V                        0x0082
#define ATKMEDIA_C                        0x008A

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	int wmId = LOWORD(wParam);
	int wmEvent = HIWORD(wParam);
	BYTE vKeyCode = 0;
	switch (message)
	{
	case WM_COMMAND:
		switch (wmId)
		{
		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;

		case ATKMEDIA_MESSAGE:
			switch(wmEvent)
			{
			case ATKMEDIA_PLAY:
                vKeyCode = VK_MEDIA_PLAY_PAUSE;
				break;
			case ATKMEDIA_STOP:
				vKeyCode = VK_MEDIA_STOP;
				break;
			case ATKMEDIA_NEXT:
				vKeyCode = VK_MEDIA_NEXT_TRACK;
				break;
			case ATKMEDIA_PREV:
				vKeyCode = VK_MEDIA_PREV_TRACK;
				break;
			case ATKMEDIA_ENTER:
				RunProc(enter_cmd);
				break;
			}
			keybd_event(vKeyCode, 0,               0, 0); //Key down
			keybd_event(vKeyCode, 0, KEYEVENTF_KEYUP, 0); //Key up (quite useless)
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
        switch (wParam)
        {
        case ATKMEDIA_SPACE:
            RunProc(space_cmd);
            break;
        case ATKMEDIA_C:
             RunProc(c_cmd);
            break;
        case ATKMEDIA_V:
            RunProc(v_cmd);
            break;
        }
        return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
